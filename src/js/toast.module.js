var myApp = myApp || {};

myApp.Toast = (function(window) {


    var _TOASTContainer = '.successFailContainer';

    function getToastContainer() {
        var $target = $(_TOASTContainer);
        var CREATED_TARGET;
        if ($target.length === 0) {
            //throw (getExceptions().elementMissing);
            CREATED_TARGET = document.createElement("div");
            CREATED_TARGET.classList.add("successFailContainer");
            document.body.appendChild(CREATED_TARGET);
            return $(CREATED_TARGET);
        } else {
            return $target;
        }

    }

    function init(configs) {

        var toast = getToastContainer();
        var callback = (typeof configs.callback == "function") ? configs.callback : function() { console.warn("Callback is not defined in configs ;)") };
        toast.css("backgroundColor", configs.backgroundColor)
            .slideDown(200)
            .html("<h3>" + configs.message + "</h3>")
            .delay(configs.timeout)
            .fadeOut(300, callback);
    }

    return {
        init: init
    }

})(this);